package com.cursehq.extension.thebridge.events;

import com.cursehq.core.Core;
import com.cursehq.core.api.event.CoreEvent;
import com.cursehq.core.api.user.User;
import com.cursehq.extension.thebridge.TheBridge;
import com.cursehq.extension.thebridge.arena.Arena;
import com.cursehq.extension.thebridge.arena.signs.ArenaSign;
import com.cursehq.extension.thebridge.arena.signs.type.SignType;
import org.bukkit.block.Block;
import org.bukkit.event.block.SignChangeEvent;

public class SignChange extends CoreEvent<SignChangeEvent> {
    private final TheBridge bridge;

    public SignChange(TheBridge bridge) {
        super(SignChangeEvent.class);
        this.bridge = bridge;
    }

    //[TheBridge]
    //Join
    //ArenaName
    //Waiting... (1/16)
    public void onExecute(SignChangeEvent event) {
        User user = getCore().getUsers().get(event.getPlayer());
        Block block = event.getBlock();
        if (!event.getLine(0).equalsIgnoreCase("(thebridge)"))
            return;
        if (!user.hasRank("administrator"))
            return;

        SignType type = SignType.JOIN;
        Arena arena = bridge.getArenas().get(event.getLine(1));
        if (arena == null) {
            user.sendMsg("&7Invalid arena!");
            return;
        }

        Core.log("");
        Core.log("&7Added ArenaSign at " + block.getX() + " " + block.getY() + " " + block.getZ() + " " + block.getType().name());
        Core.log("");

        arena.getSigns().add(new ArenaSign(event.getBlock().getLocation(), type));
        event.setLine(0, getCore().getUtils().colorize("&8&l(&a&lTheBridge&8&l)"));
        event.setLine(1, getCore().getUtils().colorize("&b" + arena.getName()));
        event.setLine(2, getCore().getUtils().colorize("&a" + arena.getStatus().getText()));
        event.setLine(3, getCore().getUtils().colorize("&b" + arena.getCurrentPlayers() + "&8/&b" + arena.getMaxPlayers()));

        Core.log("&7Sent a ping out to " + arena.getName());
        arena.ping();
        user.sendMsg("&7You created an arena sign!");
    }
}
