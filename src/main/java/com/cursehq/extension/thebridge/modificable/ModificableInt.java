package com.cursehq.extension.thebridge.modificable;

public class ModificableInt {
    private int i;

    public ModificableInt(int i) {
        this.i = i;
    }

    public int get() {
        return i;
    }
    public void set(int i) {
        this.i = i;
    }
    public void add() {
        i++;
    }
    public void remove() {
        i--;
    }
}
