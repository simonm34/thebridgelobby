package com.cursehq.extension.thebridge.communicator.listeners;

import com.cursehq.core.api.communicator.listener.CommunicatorListener;
import com.cursehq.extension.thebridge.TheBridge;
import com.cursehq.extension.thebridge.arena.Arena;
import org.bukkit.block.Sign;

public class BridgeUserJoinArena extends CommunicatorListener {
    private final TheBridge bridge;

    public BridgeUserJoinArena(TheBridge bridge) {
        super("BRIDGE_USER_JOIN_ARENA");
        this.bridge = bridge;
    }

    public void onMessageReceive(String msg, String from) {
        Arena arena = bridge.getArenas().get(msg);
        if (arena == null)
            return;
        arena.addPlayer();
        arena.getSigns().forEach(arenaSign -> {
            Sign sign = arenaSign.getSign();
            if (sign == null)
                return;
            sign.setLine(3, getCore().getUtils().colorize("&b" + arena.getCurrentPlayers() + "&8/&b" + arena.getMaxPlayers() + "&8)"));
            sign.update();
        });
    }
}
