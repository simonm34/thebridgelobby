package com.cursehq.extension.thebridge;

import com.cursehq.core.api.extension.Extension;
import com.cursehq.extension.thebridge.arena.collection.Arenas;
import com.cursehq.extension.thebridge.communicator.listeners.BridgeStatusChangeArena;
import com.cursehq.extension.thebridge.communicator.listeners.BridgeUserJoinArena;
import com.cursehq.extension.thebridge.communicator.listeners.BridgeUserLeaveArena;
import com.cursehq.extension.thebridge.events.BlockBreak;
import com.cursehq.extension.thebridge.events.PlayerInteract;
import com.cursehq.extension.thebridge.events.SignChange;

public class TheBridge extends Extension {
    private Arenas arenas;

    public void onEnable() {
        loadDepends();
        loadEvents();
        loadCommunicatorListeners();

        getArenas().load(this);
        getArenas().loadSigns(this);
    }
    public void onDisable() {
        getArenas().save(this);
    }

    private void loadDepends() {
        arenas = new Arenas();
    }
    private void loadEvents() {
        new PlayerInteract(this);
        new SignChange(this);
        new BlockBreak(this);
    }
    private void loadCommunicatorListeners() {
        new BridgeUserJoinArena(this);
        new BridgeUserLeaveArena(this);
        new BridgeStatusChangeArena(this);
    }

    public Arenas getArenas() {
        return arenas;
    }
}
