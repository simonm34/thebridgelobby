package com.cursehq.extension.thebridge.events;

import com.cursehq.core.api.event.CoreEvent;
import com.cursehq.core.api.user.User;
import com.cursehq.extension.thebridge.TheBridge;
import com.cursehq.extension.thebridge.arena.Arena;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteract extends CoreEvent<PlayerInteractEvent> {
    private final TheBridge bridge;

    public PlayerInteract(TheBridge bridge) {
        super(PlayerInteractEvent.class);
        this.bridge = bridge;
    }

    public void onExecute(PlayerInteractEvent event) {
        User user = getCore().getUsers().get(event);
        Action action = event.getAction();
        Block block = event.getClickedBlock();

        if (action != Action.RIGHT_CLICK_BLOCK)
            return;
        if (!(block.getState() instanceof Sign))
            return;

        Sign sign = (Sign) block.getState();
        if (!sign.getLine(0).equals(getCore().getUtils().colorize("&8&l(&a&lTheBridge&8&l)")))
            return;

         Arena arena = bridge.getArenas().get(ChatColor.stripColor(sign.getLine(1)));
         if (arena == null)
             return;

         switch (arena.getStatus()) {
             case IN_GAME:
                 user.sendMsg("&7The arena &a" + arena.getName() + " &7has already started!");
                 return;
             case RESETTING:
                 user.sendMsg("&7The arena &a" + arena.getName() + " &7is resetting and will be ready soon!");
                 return;
             case OFFLINE:
                 user.sendMsg("&7The arena &a" + arena.getName() + " &7is not online!");
                 return;
             case SETTING_UP:
                 if (!user.hasRank("developer")) {
                     user.sendMsg("&7This arena is not properly setup, please contact a developer!");
                     return;
                 }
                 break;
             case WAITING:
             case STARTING:
                 break;
         }
         if (arena.getCurrentPlayers() >= arena.getMaxPlayers()) {
             user.sendMsg("&7Arena &a" + arena.getName() + " &7has reached the maximum players!");
             return;
         }
        user.sendMsg("&7Joining arena &a" + arena.getName() + "&7!");
        getCore().getCommunicator().sendMessage("PROXY_SERVER_SEND", user.getName() + "," + arena.getRedisName(), "proxy");
    }
}
