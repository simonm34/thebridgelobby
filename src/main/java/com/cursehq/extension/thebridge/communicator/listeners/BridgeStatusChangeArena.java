package com.cursehq.extension.thebridge.communicator.listeners;

import com.cursehq.core.Core;
import com.cursehq.core.api.communicator.listener.CommunicatorListener;
import com.cursehq.extension.thebridge.TheBridge;
import com.cursehq.extension.thebridge.arena.Arena;
import com.cursehq.extension.thebridge.arena.status.ArenaStatus;
import org.bukkit.block.Sign;

public class BridgeStatusChangeArena extends CommunicatorListener {
    private final TheBridge bridge;

    public BridgeStatusChangeArena(TheBridge bridge) {
        super("BRIDGE_STATUS_CHANGE_ARENA");
        this.bridge = bridge;
    }

    public void onMessageReceive(String msg, String from) {
        String[] strings = msg.split(",");
        if (strings.length != 2)
            return;

        Arena arena = bridge.getArenas().get(strings[0]);
        if (arena == null)
            return;

        ArenaStatus status = parseArenaStatus(strings[1]);
        if (status == null)
            return;

        arena.setStatus(status);
        if (status == ArenaStatus.RESETTING || status == ArenaStatus.OFFLINE) {
            arena.setCurrentPlayers(0);
            arena.getSigns().forEach(arenaSign -> {
                Sign sign = arenaSign.getSign();
                if (sign == null)
                    return;
                sign.setLine(2, getCore().getUtils().colorize("&a" + status.getText()));
                sign.setLine(3, getCore().getUtils().colorize("&b" + arena.getCurrentPlayers() + "&8/&b" + arena.getMaxPlayers()));
                sign.update();
            });
            return;
        }
        arena.getSigns().forEach(arenaSign -> {
            Sign sign = arenaSign.getSign();
            if (sign == null)
                return;
            sign.setLine(2, getCore().getUtils().colorize("&a" + status.getText()));
            sign.update();
        });
    }

    private ArenaStatus parseArenaStatus(String string) {
        try {
            return ArenaStatus.valueOf(string);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }
}
