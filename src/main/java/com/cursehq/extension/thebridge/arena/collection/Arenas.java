package com.cursehq.extension.thebridge.arena.collection;

import com.cursehq.core.Core;
import com.cursehq.extension.thebridge.TheBridge;
import com.cursehq.extension.thebridge.arena.Arena;
import com.cursehq.extension.thebridge.arena.signs.ArenaSign;
import com.cursehq.extension.thebridge.arena.signs.type.SignType;
import com.cursehq.extension.thebridge.modificable.ModificableInt;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;

import java.util.HashSet;
import java.util.concurrent.atomic.AtomicInteger;

public class Arenas extends HashSet<Arena> {
    public ArenaSign getSign(Location loc) {
        for (Arena arena : this) {
            for (ArenaSign sign : arena.getSigns()) {
                Block block = sign.getBlock();
                if (block.getLocation().getWorld() != loc.getWorld())
                    continue;
                if (block.getLocation().getX() != loc.getX())
                    continue;
                if (block.getLocation().getY() != loc.getY())
                    continue;
                if (block.getLocation().getZ() != loc.getZ())
                    continue;
                return sign;
            }
        }
        return null;
    }
    public Arena get(Location loc) {
        for (Arena arena : this) {
            for (ArenaSign sign : arena.getSigns()) {
                Block block = sign.getBlock();
                if (block.getLocation().getWorld() != loc.getWorld())
                    continue;
                if (block.getLocation().getX() != loc.getX())
                    continue;
                if (block.getLocation().getY() != loc.getY())
                    continue;
                if (block.getLocation().getZ() != loc.getZ())
                    continue;
                return arena;
            }
        }
        return null;
    }
    public Arena get(String name) {
        return stream()
                .filter(arena -> arena.getName().equalsIgnoreCase(name))
                .findAny()
                .orElse(null);
    }

    public void load(TheBridge bridge) {
        Configuration data = bridge.getConfig();
        ConfigurationSection section = data.getConfigurationSection("arenas");
        section.getKeys(false).forEach(name -> {
            String redis = data.getString("arenas." + name + ".redis");
            int minPlayers = data.getInt("arenas." + name + ".min players");
            int maxPlayers = data.getInt("arenas." + name + ".max players");
            add(new Arena(Core.getCore().getUtils().capitalize(name), redis, minPlayers, maxPlayers));
        });
        Core.log("&8(&aThe&2Bridge&8) &7Successfully loaded &a" + size() + " &7arenas!");

        forEach(Arena::ping);
        Core.log("&8(&aThe&2Bridge&8) &7Successfully pinged all arenas!");
    }
    public void loadSigns(TheBridge bridge) {
        Configuration data = bridge.getConfig();
        ConfigurationSection section = data.getConfigurationSection("signs");
        if (section != null) {
            section.getKeys(false).forEach(name -> {
                data.getConfigurationSection("signs." + name).getKeys(false).forEach(id -> {
                    String path = "signs." + name + "." + id;
                    Arena arena = bridge.getArenas().get(name);
                    if (arena == null)
                        return;
                    SignType type = SignType.JOIN;
                    double x = data.getDouble(path + ".x");
                    double y = data.getDouble(path + ".y");
                    double z = data.getDouble(path + ".z");
                    World world = bridge.getServer().getWorld(data.getString(path + ".world"));
                    if (world == null)
                        return;
                    Location loc = new Location(world, x, y, z);
                    Block block = loc.getBlock();
                    if (block.getType() == Material.SIGN || block.getType() == Material.WALL_SIGN)
                        arena.getSigns().add(new ArenaSign(loc, type));
                });
            });
        }
        Core.log("&8(&aThe&2Bridge&8) &7Successfully loaded &a" + size() + " &7arena signs!");
    }
    public void save(TheBridge bridge) {
        Configuration data = bridge.getConfig();
        data.set("signs", null);
        forEach(arena -> {
            ModificableInt i = new ModificableInt(0);
            arena.getSigns().forEach(sign -> {
                Location loc = sign.getBlock().getLocation();
                data.set("signs." + arena.getName() + "." + i.get() + ".type", sign.getType().name().toLowerCase());
                data.set("signs." + arena.getName() + "." + i.get() + ".x", loc.getX());
                data.set("signs." + arena.getName() + "." + i.get() + ".y", loc.getY());
                data.set("signs." + arena.getName() + "." + i.get() + ".z", loc.getZ());
                data.set("signs." + arena.getName() + "." + i.get() + ".world", loc.getWorld().getName());
                i.add();
            });
        });
        bridge.saveConfig();
        Core.log("&8(&aThe&2Bridge&8) &7Successfully saved &a" + size() + " &7arena signs!");
    }
}
