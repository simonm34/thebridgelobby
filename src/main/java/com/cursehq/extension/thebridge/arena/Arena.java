package com.cursehq.extension.thebridge.arena;

import com.cursehq.core.Core;
import com.cursehq.extension.thebridge.arena.signs.ArenaSign;
import com.cursehq.extension.thebridge.arena.status.ArenaStatus;

import java.util.HashSet;
import java.util.Set;

public class Arena {
    private final Set<ArenaSign> signs;
    private final String name;
    private final String redisName;
    private final int minPlayers;
    private final int maxPlayers;
    private int currentPlayers;
    private ArenaStatus status;

    public Arena(String name, String redisName, int minPlayers, int maxPlayers) {
        this.signs = new HashSet<>();
        this.name = name;
        this.redisName = redisName;
        this.minPlayers = minPlayers;
        this.maxPlayers = maxPlayers;
        this.currentPlayers = 0;
        status = ArenaStatus.OFFLINE;
    }

    public Set<ArenaSign> getSigns() {
        return signs;
    }

    public String getName() {
        return name;
    }

    public String getRedisName() {
        return redisName;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public int getCurrentPlayers() {
        return currentPlayers;
    }
    public void setCurrentPlayers(int currentPlayers) {
        this.currentPlayers = currentPlayers;
    }
    public void addPlayer() {
        currentPlayers++;
    }
    public void removePlayer() {
        currentPlayers--;
    }

    public ArenaStatus getStatus() {
        return status;
    }
    public void setStatus(ArenaStatus status) {
        this.status = status;
    }

    public void ping() {
        Core.getCore().getCommunicator().sendMessage("BRIDGE_PING_ARENA", name);
    }
}
