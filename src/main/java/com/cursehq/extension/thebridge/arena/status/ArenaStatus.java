package com.cursehq.extension.thebridge.arena.status;

public enum ArenaStatus {
    WAITING("Waiting"),
    STARTING("Starting"),
    IN_GAME("In Game"),
    RESETTING("Resetting"),
    OFFLINE("Offline"),
    SETTING_UP("Setting Up");

    private final String text;

    ArenaStatus(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
