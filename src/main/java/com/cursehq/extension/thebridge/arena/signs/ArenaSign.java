package com.cursehq.extension.thebridge.arena.signs;

import com.cursehq.core.Core;

import com.cursehq.extension.thebridge.arena.signs.type.SignType;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

public class ArenaSign {
    private final Location loc;
    private final SignType type;

    public ArenaSign(Location loc, SignType type) {
        this.loc = loc;
        this.type = type;
    }

    public Block getBlock() {
        return loc.getBlock();
    }
    public SignType getType() {
        return type;
    }
    public Sign getSign() {
        Block block = getBlock();
        if (block.getType() != Material.SIGN && block.getType() != Material.WALL_SIGN) {
            Core.log("&8(TheBridge) &7An arena sign has been removed, the block is no longer a sign. Contact a developer!");
            return null;
        }
        return (Sign) block.getState();
    }
}
