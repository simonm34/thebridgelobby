package com.cursehq.extension.thebridge.events;

import com.cursehq.core.api.event.CoreEvent;
import com.cursehq.core.api.user.User;
import com.cursehq.extension.thebridge.TheBridge;
import com.cursehq.extension.thebridge.arena.Arena;
import com.cursehq.extension.thebridge.arena.signs.ArenaSign;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak extends CoreEvent<BlockBreakEvent> {
    private final TheBridge bridge;

    public BlockBreak(TheBridge bridge) {
        super(BlockBreakEvent.class);
        this.bridge = bridge;
    }

    public void onExecute(BlockBreakEvent event) {
        User user = getCore().getUsers().get(event.getPlayer());
        Block block = event.getBlock();
        if (block.getType() != Material.SIGN)
            return;

        Arena arena = bridge.getArenas().get(block.getLocation());
        if (arena == null)
            return;

        ArenaSign sign = bridge.getArenas().getSign(block.getLocation());
        if (sign == null)
            return;
        arena.getSigns().remove(sign);
        user.sendMsg("&7You removed a bridge sign!");
    }
}
